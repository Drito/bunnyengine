# e_image_loader.nim
{.link: "Libs/Lodepng/lodepng.o".}

import e_objects

proc lodepng_decode32_file(image:ptr seq[char], width, height:ptr GLint, filename:cstring):uint {.importc.}

###################################################################################################

proc load_data*(image:Image, path:string) =
    var error = lodepng_decode32_file(addr image.data, addr image.width, addr image.height, cstring(path))
    if bool(error):
        echo "\"", image.file, "\"", " not loaded (load_data)"

#proc free_data*(image:Image) =
#    stbi_image_free(addr image.data)
