# e_image_loader.nim

import e_objects

proc stbi_load(filename:cstring, x, y:ptr GLint, comp:ptr GLint, req_comp:GLint):seq[char] {.importc.}

proc stbi_image_free(retval_from_stbi_load: pointer) {.importc.}


###################################################################################################

proc load_data*(image:Image, path:string) =
    image.data = stbi_load(path, addr image.width, addr image.height, 
                           addr image.channelCount, 4)
    if image.data.len == 0:
        echo "\"", image.file, "\"", " not loaded (load_data)"

#proc free_data*(image:Image) =
#    stbi_image_free(addr image.data)
