#bunnymark.nim

import strutils, times, random,
       e_image_loader, e_engine, e_objects

type Bunny = tuple
    ent:Entity
    velocity:Vec2f
    updates:int

const
    gravity = vec2f(0.0, 1.0).norm
    gravityForce = 0.1

var
    bunnyMark = new_app()
    mat = material()
    population:seq[Bunny]
    winHeight = float(bunnyMark.baseWindow.height)
    winWidth = float(bunnyMark.baseWindow.width)
    bunnyWidth:float
    bunnyHeigth:float
    impulse = 9.0
    count:int

mat.tex.load_data("Image/bunny.png")
mat.coordCorrection = - 0.14

bunnyMark.fpsTarget = 60

bunnyWidth = float(mat.tex.width)
bunnyHeigth = float(mat.tex.height)

proc new_bunny():Bunny =
    result.ent = bunnyMark.add_entity(@[transform2D, drawing])
    result.ent.shape = rect(bunnyWidth, bunnyHeigth)
    result.ent.material = mat
    result.velocity = vec2f(1.0, 0.0).norm * (impulse * rand[float](0.2..0.4))

proc update_bunnies() =
    for b in 0..population.high:
        var bunnyPos = vec2f(population[b].ent.position)
        if (bunnyPos.y + bunnyHeigth > winHeight):
            population[b].velocity = vec2f(rand[float](-1.0..1.0), -5.0).norm * impulse

        elif (bunnyPos.x + bunnyWidth > winWidth):
            population[b].velocity = vec2f(-1.0, 0.0).norm

        elif (bunnyPos.x < 0.0):
            population[b].velocity = vec2f(1.0, 0.0).norm

        population[b].velocity = (population[b].velocity + (gravity * gravityForce)) 
        population[b].ent.position = translation(bunnyPos + (population[b].velocity))

    if bunnyMark.framerate > (bunnyMark.fpsTarget - 1):
        population.add(new_bunny())
        if population.len == count + 100:
            echo population.len, " bunnies"
            count += 100
            echo bunnyMark.delta

    bunnyMark.baseWindow.title = "F.P.S: " & intToStr(bunnyMark.framerate)

bunnyMark.updateProc = update_bunnies
process(bunnyMark)
echo population.len, " bunnies"


