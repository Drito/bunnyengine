# e_components.nim

import e_objects, e_system, e_graphics

type
    Window* = object
        ## An O.S window.
        #sysWin:SysWindow
        #viewport:Viewport
        width:int
        height:int
        title:string
        currentContext:bool
        vSync:bool

    Drawing* = object
        ## A 2D graphic drawing.
        #buffer:Buffer
        shape:Shape
        material:Material
        #transform:Mat4x4

    Transform2D* = object
        ## Define the transformations applied to the corresponding objects.
        origin:Vec2f
        scale:Mat4x4
        rotation:Mat4x4
        position:Mat4x4

type Set* = enum
    window
    drawing
    transform2D

type 
    Entity* = ref object
        id:int
        name:string
        inUse:bool
        initialized:bool
        has:tuple[window:bool,
                  drawing:bool,
                  transform2D:bool]
        world:ptr ObjectBase

    ObjectBase* = tuple
        windowSet:seq[Window]
        transform2DSet:seq[Transform2D]
        drawingSet:seq[Drawing]
        graphicObjs:seq[Buffer]
        viewports:seq[Viewport]
        windowObjs:seq[WindowObj]
        #timerObjs:seq[Timer]

    ComponentCount* = tuple
        window:int
        drawing:int
        transform2D:int

proc contains(arr:seq[Set], s:Set):bool =
    for i in arr:
        if i == s:
            return true

proc countComponents(c:var ComponentCount, entSeq:var seq[Entity]) =
    c.window = 0
    c.drawing = 0
    c.transform2D = 0
    for i in entSeq:
        if i.has.window:
            c.window += 1
        if i.has.drawing:
            c.drawing += 1
        if i.has.transform2D:
            c.transform2D += 1

proc id*(ent:Entity):int =
    return ent.id

proc resize_seqs(o:var ObjectBase, newSize:int) =
        for i in o.fields:
            try:
                i.setLen(newSize)
            except:
                echo "(╯°□°)╯ ", newSize

proc clean_components*(o:var ObjectBase, ents:var seq[Entity]) =
    # Remove unused components, the entity and update the entity id.
    var removeQueue:seq[int]
    for i in 0..ents.high:
        if ents[i].inUse == false:
            removeQueue.add(i)
            for j in o.fields:
                j.delete(i)

    for i in removeQueue:
        ents.delete(i)
        #sc.count.entity -= 1

    removeQueue.setLen(0)
    for i in 0..ents.high:
        ents[i].id = i

proc `inUse=`*(ent:Entity, value:bool) =
    ent.inUse = value

proc inUse*(ent:Entity):bool =
    return ent.inUse

proc name*(ent:Entity):string =
    return ent.name

proc width*(ent:Entity):int =
    ## Window width in pixels.
    return ent.world.windowSet[ent.id].width

proc `height`*(ent:Entity):int =
    ## Window height in pixels.
    ent.world.windowSet[ent.id].height

proc `title=`*(ent:Entity, text:string) =
    ## Set the string displayed on top of the window.
    ent.world.windowSet[ent.id].title = text
    #win.requestUpdate = true
    
proc `title`*(ent:Entity):string =
    ## Return the string displayed on top of the window.
    ent.world.windowSet[ent.id].title
    
proc `currentContext=`*(ent:Entity, value:bool) =
    ## Select the window for the rendering.
    ent.world.windowSet[ent.id].currentContext = value
    #win.requestUpdate = true

proc `currentContext`*(ent:Entity):bool =
    ## Return the value who define if the window holds the current context.
    ent.world.windowSet[ent.id].currentContext

proc `v_sync=`*(ent:Entity, value:bool) =
    ## enable or disable v sync
    ent.world.windowSet[ent.id].vSync = value

proc `shape=`*(ent:Entity, sh:Shape) =
    ## Assign the 2D shape of the drawing.
    ent.world.drawingSet[ent.id].shape = sh

proc `shape`*(ent:Entity):Shape =
    ## Gets the shape object.
    ent.world.drawingSet[ent.id].shape

proc `material=`*(ent:Entity, mat:Material) =
    ## Assign a material who sets graphic looking.
    ent.world.drawingSet[ent.id].material = mat

proc `vertexColor=`*(ent:Entity, vertCol:seq[Rgba]) =
    ## Assign colors by vertex. 
    ## The "VertexColModulate" material value must be set to 1.0 to fully displays the vertex colors.
    ent.world.drawingSet[ent.id].shape.vertexColor = vertCol

proc `origin=`*(ent:Entity, vec:Vec2f) =
    ## Update a shape with a new origin point.
    ent.world.transform2DSet[ent.id].origin = vec

proc origin*(ent:Entity):Vec2f =
    ## Gets the shape origin.
    return ent.world.transform2DSet[ent.id].origin

proc `rotation=`*(ent:Entity, ro:Mat4x4) =
    ## Update the shape rotation
    ent.world.transform2DSet[ent.id].rotation = ro

proc rotation*(ent:Entity):Mat4x4 =
    ## Gets the current shape rotation.
    return ent.world.transform2DSet[ent.id].rotation

proc `position=`*(ent:Entity, pos:Mat4x4) =
    ## Update a shape with a new position.
    ent.world.transform2DSet[ent.id].position = pos
    #ent.world.transform2DSet[ent.id].matrix[12] = vec.x
    #ent.world.transform2DSet[ent.id].matrix[13] = vec.y

proc position*(ent:Entity):var Mat4x4 =
    ## Gets a shape position.
    return ent.world.transform2DSet[ent.id].position
    #result.x = ent.world.transform2DSet[ent.id].matrix[12]
    #result.y = ent.world.transform2DSet[ent.id].matrix[13]

proc `scale=`*(ent:Entity, sc:Mat4x4) =
    ## Update the scale.
    ent.world.transform2DSet[ent.id].scale = sc
    
proc scale*(ent:Entity):Mat4x4 =
    ## Gets the current scale.
    return ent.world.transform2DSet[ent.id].scale
   



