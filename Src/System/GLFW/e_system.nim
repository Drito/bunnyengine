# System/GLFW/e_system.nim

#type GLFWmonitor = ptr object #{.importc.}

import e_objects

const
    GLFW_NOT_INITIALIZED = 0x00010001

const
    GLFW_CONTEXT_VERSION_MAJOR = 0x00022002
    GLFW_CONTEXT_VERSION_MINOR = 0x00022003
    GLFW_OPENGL_PROFILE = 0x00022008
    GLFW_OPENGL_CORE_PROFILE = 0x00032001

const
    GLFW_RESIZABLE =0x00020003

#GLFW lib######################################################################

proc glfwWindowHint(hint:int, value:int) {.importc.}

proc glfwInit():cint {.importc.}

proc glfwCreateWindow(width, height:int, title:cstring = "GLFW Window", monitor:pointer = nil, 
    share:pointer = nil):pointer {.importc.}

proc glfwMakeContextCurrent(window:pointer) {.importc.}

proc glfwSwapInterval(interval:int) {.importc.}

proc glfwSetWindowTitle(window:pointer, text:cstring) {.importc.}

proc glfwSwapBuffers(window:pointer) {.importc.}

proc glfwPollEvents() {.importc.}

proc glfwWindowShouldClose(window:pointer):cint {.importc.}

proc glfwDestroyWindow(window:pointer) {.importc.}

proc glfwTerminate() {.importc.}

proc glfwGetProcAddress(procName:cstring):pointer {.importc.}

#System########################################################################

type WindowObj* = ref object
    #indice:int
    handle:pointer
    #width:int
    #height:int
    #title:string
    #currentContext:bool
    #vSync:bool

type System* = ref object
    err:cint
    #winSet:seq[SysWindow]
    initialized:bool
    
proc initialized*(sys:System):bool =
    return sys.initialized

proc `err`*(sys:System):int =
    return sys.err

proc new_system*():System =
    result = new System
    result.err = glfwInit()
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3)
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3)
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE)
    glfwWindowHint(GLFW_RESIZABLE, 0)
    if result.err == 0:
        echo "init_system failed (new_App)"
    else:
        result.initialized = true

proc stop_system*() =
    glfwTerminate()
    
proc update*(win:WindowObj) =
    glfwSwapBuffers(win.handle)

proc process*(sys:System) =
    glfwPollEvents()

proc context_to*(win:WindowObj) =
    glfwMakeContextCurrent(win.handle)

proc pop_window*(winObj:WindowObj, width, height:int) =
     winObj.handle = glfwCreateWindow(width, height)
     #sys.winSet.add(win)
     #win.indice = sys.winSet.high

proc close_request*(win:WindowObj):bool =
    return bool(glfwWindowShouldClose(win.handle))

proc remove*(win:WindowObj) =
    glfwDestroyWindow(win.handle)
    #sys.winSet.delete(win.indice)
    echo "A window is removed (sys remove)"

proc set_vsync*(value:bool) =
    glfwSwapInterval(int(value))

proc set_window_title*(win:WindowObj, text:cstring) =
    glfwSetWindowTitle(win.handle, text)

#proc set_resizable*(value:bool) =
#    glfwWindowHint(GLFW_RESIZABLE, int(value))
    
#proc get_gl_address*(procName:string):pointer =
#    return glfwGetProcAddress(procName)

