//e_gl_loader.h
#ifndef GL_LOADER
#   define GL_LOADER


#	include <../../../Libs/Glad/include/glad/gla.h> /*j'ecris le chemin direct 
											ici sinon je vais devenir fou*/
#	include <GLFW/glfw3.h>
#	include <stdio.h>

char load_gl()
{
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    {
        printf("Failed to initialize GLAD");
        return -1;
    }
}


#endif