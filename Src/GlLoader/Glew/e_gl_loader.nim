import e_objects

const GLEW_OK = char(0)

proc glewInit():char {.importc.}

proc glewGetErrorString(err:char):cstring {.importc.}

###############################################################################

proc load_gl*():bool =
    var initResult = glewInit()
    if initResult != GLEW_OK:
        echo "Glew: ", glewGetErrorString(initResult), " (load_gl)"
        return false
    else:
        return true
 

