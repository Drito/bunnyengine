# e_engine.nim

import times, os, e_objects, e_image_loader

include e_components

const
    #MAX_ENTITY_COUNT = 10000
    MAX_WIN_COUNT = 1
    WINDOW_WIDTH = 800
    WINDOW_HEIGHT = 600
    DEFAULT_TITLE = "Bunny engine"
    DEFAULT_COLOR = Rgba(r:0.1, g:0.1, b:0.1, a:1.0)
    DEFAULT_FPS = 60

let 
    defaultMaterial = material(rgba(1.0, 1.0, 1.0, 1.0))
    DEFAULT_RECT = rect(100.0, 100.0)

#App###########################################################################

type App* = ref object
    ## The application. A single application object is allowed.
    graphics:Graphics
    system:System
    entitySeq:seq[Entity]
    world:ObjectBase
    count:ComponentCount
    updateProc*:proc()
    onCloseProc*:proc()
    baseWindow:Entity
    framerate:int
    fpsTarget:int
    delta:float
    initialized:bool

proc default_proc() = discard

proc display_count*(app:App) =
    echo "{ \nWindows: ",
        app.count.window,
        "\nDrawings: ",
        app.count.drawing,
        "\nTransform2D ",
        app.count.transform2D,
        "\n}"

proc add_entity*(app:App, cptSet:seq[Set], st:string = "unnamed entity"):Entity =
    result = Entity()
    app.entitySeq.add(result)
    result.world = addr app.world
    result.id = app.entitySeq.high
    result.name = st
    app.world.resize_seqs(app.entitySeq.len)
    if cptSet.contains(window):
        if app.count.window < MAX_WIN_COUNT:
            app.world.windowSet[result.id] = Window()
            app.world.windowSet[result.id].width = WINDOW_WIDTH
            app.world.windowSet[result.id].height = WINDOW_HEIGHT
            app.world.windowSet[result.id].title = DEFAULT_TITLE
            app.world.windowSet[result.id].vSync = true
            app.world.viewports[result.id] = viewport()
            app.world.windowObjs[result.id] = WindowObj()
            result.has.window = true
        else:
            echo "Window discarded, max windowSet reached (add_entity)"

    if cptSet.contains(transform2D):
        app.world.transform2DSet[result.id] = Transform2D()
        #app.world.transform2DSet[result.id].matrix = identityMat4x4
        app.world.transform2DSet[result.id].scale = scaling(vec2f(1.0, 1.0))
        app.world.transform2DSet[result.id].position = identityMat4x4
        app.world.transform2DSet[result.id].rotation = identityMat4x4
        result.has.transform2D = true

    if cptSet.contains(drawing):
        app.world.drawingSet[result.id] = Drawing()
        app.world.graphicObjs[result.id] = buffer()
        app.world.drawingSet[result.id].shape = DEFAULT_RECT
        app.world.drawingSet[result.id].material = defaultMaterial
        result.has.drawing = true

    countComponents(app.count, app.entitySeq)
    result.inUse = true

proc init(app:App, ent:Entity) =
    # Starts an entity for processing.
    if ent.has.window:
        app.world.windowObjs[ent.id].pop_window(app.world.windowSet[ent.id].width,
                                                app.world.windowSet[ent.id].height)

        if app.count.window == 1:
            context_to(app.world.windowObjs[ent.id])
            app.world.windowSet[ent.id].currentContext = true
        
        app.world.viewports[ent.id].init(app.graphics,
                                         GLuint(app.world.windowSet[ent.id].width),
                                         GLuint(app.world.windowSet[ent.id].height))

    if app.count.window < 1:
        echo "No window ! (app init ent)"

    if ent.has.drawing:
        app.world.graphicObjs[ent.id].shape_to_vertices(app.world.drawingSet[ent.id].shape,
                                                       app.world.drawingSet[ent.id].material)
        app.graphics.add_vert(app.world.graphicObjs[ent.id])

        if app.world.drawingSet[ent.id].material.tex.data.len != 0:
            app.world.graphicObjs[ent.id].add_texture(app.world.drawingSet[ent.id].material.tex)
            #free_data(app.world.drawingSet[ent.id].material.tex)

    ent.initialized = true

proc new_App*():App =
    ## Creates an application, add and init a first entity who holds a window.
    result = new App
    result.updateProc = default_proc
    result.onCloseProc = default_proc
    result.baseWindow = result.add_entity(@[window, transform2D], "baseWindow")
    result.count.countComponents(result.entitySeq)
    result.system = new_system()
    result.graphics = new_graphics(Rgba(r:0.1, g:0.1, b:0.1, a:1.0))
    result.fpsTarget = DEFAULT_FPS
    result.init(result.baseWindow)
    init(result.graphics)
    # Finish the the base window initialization.
    result.world.viewports[result.baseWindow.id].init(result.graphics,
        GLuint(result.world.windowSet[result.baseWindow.id].width),
        GLuint(result.world.windowSet[result.baseWindow.id].height))
    if result.system.initialized == true and result.graphics.initialized == true:
        result.initialized = true

#[
proc init(app:App) =
    app.init(app.baseWindow)
    init(app.graphics)
    # Finish the the base window initialization.
    app.world.viewports[app.baseWindow.id].init(app.graphics,
                                     GLuint(app.world.windowSet[app.baseWindow.id].width),
                                     GLuint(app.world.windowSet[app.baseWindow.id].height))
    if app.system.initialized == true and app.graphics.initialized == true:
        app.initialized = true
]#
proc baseWindow*(app:App):Entity =
    return app.baseWindow

proc world*(app:App):var ObjectBase =
    app.world

proc `clearColor=`*(app:App, color:Rgba) =
    ## Set the clear color of the application.
    app.clearColor = color

proc `fpsTarget=`*(app:App, target:int) =
    ## Set the max framerate in Frame Per Seconds.
    ## The framerate can't be greater than v_sync.
    if target > 0:
        app.fpsTarget = target

proc fpsTarget*(app:App):int =
    return app.fpsTarget

proc `framerate`*(app:App):int =
    ## Gets the number of frames displayed in one second.
    return app.framerate

proc delta*(app:App):float =
    return app.delta


proc clean(app:App) =
    for i in app.entitySeq:
        if i.inUse == false:
            if i.has.window:
                remove(app.world.windowObjs[i.id])
            if i.has.drawing:
                remove(app.world.graphicObjs[i.id])

    app.world.clean_components(app.entitySeq)
    countComponents(app.count, app.entitySeq)

proc close_app(app:App) =
    if app.initialized == true:
        for i in app.entitySeq:
            i.inUse = false
        app.clean()
        stop_system()

proc update(app:App) =
    # update the components
    app.updateProc()
    for i in app.entitySeq:
        if i.has.window:
            set_window_title(app.world.windowObjs[i.id], app.world.windowSet[i.id].title)
            set_vsync(app.world.windowSet[i.id].vSync) #Fait freezer l'ordi si "system" est mal initialisé.
            update(app.world.windowObjs[i.id])

            if i.has.transform2D:
                app.world.viewports[i.id].uniformViewMtx = app.world.transform2DSet[i.id].position
            else:
                app.world.viewports[i.id].uniformViewMtx = identityMat4x4

            if close_request(app.world.windowObjs[i.id]):
                app.entitySeq[i.id].inUse = false

        if i.has.drawing:
            app.world.graphicObjs[i.id].uniformModulation = GLfloat(app.world.drawingSet[i.id].material.vertColModulation)
            app.world.graphicObjs[i.id].wireframe = app.world.drawingSet[i.id].material.wireframe

            if i.has.transform2D:
                app.world.graphicObjs[i.id].uniformModelMtx = app.world.transform2DSet[i.id].position *
                                                              app.world.transform2DSet[i.id].rotation *
                                                              app.world.transform2DSet[i.id].scale *
                                                              translation(app.world.transform2DSet[i.id].origin * -1)

var alreadyRunned = false

proc process(app:App, ent:Entity) =
    if ent.has.drawing:
        app.graphics.draw(app.world.graphicObjs[ent.id])
    
    if ent.has.window:
        update_view(app.world.viewports[ent.id])

proc process*(app:App) =
    ## Runs the application with all the added components.
    var 
        frameTime:float
        frameCount:int = 0
        sleepTime:int = 0
        loopTimer = Timer()
        targetFPS_ms = int(1000.0 / float(app.fpsTarget))
    echo "targetFPS_ms: ", targetFPS_ms
    if alreadyRunned == false: # To avoid a second launch
        alreadyRunned = true
        frameTime = epochTime()
        #app.init()
        if app.initialized:
            while app.count.window > 0:
                loopTimer.start()
                app.update()
                app.system.process()
                app.graphics.clear()
                for i in app.entitySeq:
                    if i.initialized:
                        app.process(i)
                    else:
                        app.init(i)

                app.clean()
                # get the framerate
                frameCount += 1
                if epochTime() - frameTime > 1:
                    app.framerate = frameCount
                    frameCount = 0
                    frameTime = epochTime()
                
                sleepTime = targetFPS_ms - loopTimer.elapsed_ms
                if sleepTime > 0:
                    sleep(sleepTime)
                #echo sleepTime
                app.delta = loopTimer.elapsed_sec()

        app.onCloseProc()
        app.close_app()
        echo "App stopped (app process)"


