# e_graphics.nim

import e_gl_loader, e_objects, streams, os

const 
    GL_COLOR_BUFFER_BIT = 0x00004000.GLuint
    GL_DEPTH_BUFFER_BIT = 0x00000100.GLuint
    GL_ACCUM_BUFFER_BIT = 0x00000200.GLuint
    GL_STENCIL_BUFFER_BIT = 0x00000400.GLuint
    GL_ARRAY_BUFFER = 0x8892.GLuint
    GL_ELEMENT_ARRAY_BUFFER = 0x8893.GLuint
    GL_FALSE = false.char
    GL_TRUE = true.char
    GL_STATIC_DRAW = 0x88E4.GLuint
    OGL_FLOAT = 0x1406.GLuint
    GL_VERTEX_SHADER = 0x8B31.GLuint
    GL_COMPILE_STATUS = 0x8B81.GLuint
    GL_INFO_LOG_LENGTH = 0x8B84.GLuint
    GL_FRAGMENT_SHADER = 0x8B30.GLuint
    GL_LINK_STATUS = 0x8B82.GLuint
    GL_TRIANGLES = 0x0004.GLuint
    #GL_TRIANGLE_FAN = 0x0006.GLuint
    GL_UNSIGNED_INT= 0x1405.GLuint
    GL_VERSION = 0x1F02.GLuint
    GL_FRONT_AND_BACK = 0x0408.GLuint
    GL_FILL = 0x1B02.GLuint
    GL_LINE = 0x1B01.GLuint
    GL_MAX_VERTEX_ATTRIBS = 0x8869.GLuint
    GL_SRC_ALPHA = 0x0302.GLuint
    GL_ONE_MINUS_SRC_ALPHA = 0x0303.GLuint
    GL_BLEND = 0x0BE2.GLuint
    GL_TEXTURE_2D = 0x0DE1.GLuint
    GL_TEXTURE_WRAP_S = 0x2802.GLuint
    GL_REPEAT = 0x2901.GLint
    GL_CLAMP_TO_EDGE = 0x812F.GLint
    GL_TEXTURE_WRAP_T = 0x2803.GLuint
    GL_TEXTURE_MIN_FILTER = 0x2801.GLuint
    GL_TEXTURE_MAG_FILTER = 0x2800.GLuint
    GL_LINEAR = 0x2601.GLint
    GL_RGB = 0x1907.GLint
    GL_RGBA = 0x1908.GLint
    GL_UNSIGNED_BYTE = 0x1401.GLuint
    GL_BYTE = 0x1400.GLuint

const
    POSITION_LENGTH = 3.GLint
    COLOR_LENGTH = 4.GLint
    TEX_COORD_LENGTH  = 2.GLint

let
    vertexLength = POSITION_LENGTH + COLOR_LENGTH + TEX_COORD_LENGTH
    colorDataOffset = POSITION_LENGTH * sizeof(GLfloat)
    texcoordDataOffset = (POSITION_LENGTH + COLOR_LENGTH) * sizeof(GLfloat)
    defaultRectCol:Rgba = rgb(1, 1, 1)
    
var defaultModelMatrix = translation(vec2f(0.0, 0.0))

#Opengl imports################################################################

proc glClearColor(r, g, b, a:GLfloat) {.importc.}

proc glViewport(x: GLint, y: GLint, width: GLuint, height: GLuint) {.importc.}

proc glClear(mask:GLuint) {.importc.}

proc glGetString(mask:GLuint):cstring {.importc.}

proc glGenVertexArrays(n:GLuint, arrays:ptr GLuint) {.importc.}

proc glGenBuffers(n:GLuint, buffers:ptr GLuint) {.importc.}

proc glBindVertexArray(n:GLuint) {.importc.}

proc glBindBuffer(target:GLuint, buffer:GLuint) {.importc.}

proc glBufferData(target:GLuint, size:int, data:pointer, 
                    usage:GLuint) {.importc.}

proc glVertexAttribPointer(index:GLuint, size:GLint, glType:GLuint, normalized:char, 
                            stride:GLint, glPtr:pointer) {.importc.}

proc glEnableVertexAttribArray(index:GLuint) {.importc.}

proc glCreateShader(glType:GLuint):GLuint {.importc.}

proc glShaderSource(shd: GLuint, count: GLuint, src: ptr cstring, 
                        length: ptr GLint) {.importc.}

proc glCompileShader(shd: GLuint) {.importc.}

proc glGetShaderiv(shd: GLuint, pname: GLuint, params: ptr GLint) {.importc.}

#proc glGetShaderInfoLog(shd: GLuint, bufSize: GLint, length: ptr GLint, infoLog:ptr char) {.importc.}

proc glCreateProgram():GLuint {.importc.}

proc glAttachShader(program:GLuint, shader:GLuint) {.importc.}

proc glLinkProgram(program:GLuint) {.importc.}

proc glGetProgramiv(program:GLuint, pname:GLuint, params:ptr GLint) {.importc.}

#proc glGetProgramInfoLog(program:GLuint, bufSize:GLint, length:ptr GLint, infoLog:ptr char) {.importc.}

proc glDeleteShader(shd:GLuint) {.importc.}

proc glDrawElements(mode:GLuint, count:GLint, glType:GLuint, indices: pointer) {.importc.}

proc glUseProgram(program:GLuint) {.importc.}

proc glPolygonMode(face:GLuint, mode:GLuint) {.importc.}

proc glDeleteVertexArrays(n:GLint, arrays:ptr GLuint) {.importc.}

proc glDeleteBuffers(n:GLint, buffers:ptr GLuint) {.importc.}

proc glGetIntegerv(pname:GLuint, params:ptr GLint) {.importc.}

proc glGetUniformLocation(program:GLuint, name:cstring):GLint {.importc.}

proc glUniform4f(location:GLint, v0:GLfloat, v1:GLfloat, v2:GLfloat, v3:GLfloat) {.importc.}

proc glUniform1f(location:GLint, v0:GLfloat) {.importc.}

proc glUniformMatrix4fv(location:GLint, count:GLint, transpose:char,
                        value:ptr GLfloat) {.importc.}

proc glBlendFunc(sfactor, dfactor:GLuint) {.importc.}

proc glEnable(cap:GLuint) {.importc.}

proc glGenTextures(n: GLint, textures: ptr GLuint) {.importc.}

proc glBindTexture(target: GLuint, texture: GLuint) {.importc.}

proc glTexParameteri(target: GLuint, pname: GLuint, param: GLint) {.importc.}

proc glTexImage2D(target: GLuint, level: GLint, internalformat: GLint, width: GLint, height: GLint, 
                  border: GLint, format: GLuint, `type`: GLuint, pixels: pointer) {.importc.}

proc glGenerateMipmap(target: GLuint) {.importc.}

###############################################################################

type Buffer* = ref object
    #buffSetIndice*:int
    VAO, VBO, EBO:GLuint
    texture:GLuint
    vertices:seq[GLfloat]
    indices:seq[GLuint]
    vertIdCount:GLint
    wireframe*:bool
    #uniformColor*:Rgba
    uniformModulation*:GLfloat
    #uniformTexModulation*:GLfloat
    uniformModelMtx*:Mat4x4

type Viewport* = ref object
    #viewpSetIndice*:int
    uniformProjMtx:Mat4x4
    uniformProjectionLoc:Glint
    uniformViewMtx*:Mat4x4
    uniformViewLoc:Glint

type Graphics* = ref object
    #buffSet:seq[Buffer]
    #viewportSet:seq[Viewport]
    vertSource:cstring
    fragSource:cstring
    clearColor:Rgba
    shaderProgramID:GLuint
    maxVertexAttributes:GLint
    #uniformColorLoc:Glint
    uniformModulateLoc:Glint
    #uniformTexModulateLoc:Glint
    uniformModelMtxLoc:Glint
    initialized:bool
    removeQueue:seq[int]

proc initialized*(gph:Graphics):bool =
    return gph.initialized

proc buffer*():Buffer =
    result = Buffer()
    result.uniformModelMtx = translation(vec2f(0.0, 0.0))

proc viewport*():Viewport =
    result = Viewport()
    result.uniformViewMtx = identityMat4x4   
#[
proc add_viewport*(gph:Graphics):Viewport =
    result = Viewport()
    result.uniformViewMtx = identityMat4x4
    result.uniformProjectionLoc = glGetUniformLocation(gph.shaderProgramID, "projection")
    result.uniformViewLoc = glGetUniformLocation(gph.shaderProgramID, "view")
    gph.viewportSet.add(result)
    #result.viewpSetIndice = gph.viewportSet.high
]#
proc init*(gph:Graphics) =
    if  load_gl() == true:
        if fileExists("Src/Graphics/Opengl3.3/vertex_shader") and fileExists("Src/Graphics/Opengl3.3/fragment_shader"):
            gph.vertSource = readAll(newFileStream("Src/Graphics/Opengl3.3/vertex_shader"))
            gph.fragSource = readAll(newFileStream("Src/Graphics/Opengl3.3/fragment_shader"))
            var success:GLint
            #var infoLogLength:GLint #je ne comprends pas comment utiliser ça
            #var infoLog:cstring 
            glClearColor(gph.clearColor.r, gph.clearColor.g, gph.clearColor.b, gph.clearColor.a)
            echo "GL version: ", glGetString(GL_VERSION), " (gph init)"

            # vertex
            var vertexShader = glCreateShader(GL_VERTEX_SHADER)
            glShaderSource(vertexShader, 1, addr gph.vertSource, nil)
            glCompileShader(vertexShader)
            # Check compilation errors.
            glGetShaderiv(vertexShader, GL_COMPILE_STATUS, addr success)
            #glGetShaderiv(vertexShader, GL_INFO_LOG_LENGTH, addr infoLogLength)
            if bool(success) == false:
                #glGetShaderInfoLog(vertexShader, infoLogLength, nil, addr infoLog[0])
                echo " vertex shader compilation failed (gph init)" #infoLog, "
            # fragment
            var fragmentShader = glCreateShader(GL_FRAGMENT_SHADER)
            glShaderSource(fragmentShader, 1, addr gph.fragSource, nil)
            glCompileShader(fragmentShader)
            # Check compilation errors.
            glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, addr success)
            #glGetShaderiv(fragmentShader, GL_INFO_LOG_LENGTH, addr infoLogLength)
            if bool(success) == false:
                #glGetShaderInfoLog(fragmentShader, infoLogLength, nil, addr infoLog[0])
                echo "fragment shader compilation failed (gph init)"#[infoLog, "]#
            # Shader program
            gph.shaderProgramID = glCreateProgram()
            glAttachShader(gph.shaderProgramID, vertexShader)
            glAttachShader(gph.shaderProgramID, fragmentShader)
            glLinkProgram(gph.shaderProgramID)
            # Check for linkage errors.
            glGetProgramiv(gph.shaderProgramID, GL_LINK_STATUS, addr success)
            if success == 0:
                #glGetProgramInfoLog(result, 512, nil, addr infoLog[0])
                echo "shader program linking failed (gph init)" #[infoLog, "]#
            glDeleteShader(vertexShader)
            glDeleteShader(fragmentShader)
            #gph.uniformColorLoc = glGetUniformLocation(gph.shaderProgramID, "matColor")
            gph.uniformModulateLoc = glGetUniformLocation(gph.shaderProgramID, "modulate")
            #gph.uniformTexModulateLoc = glGetUniformLocation(gph.shaderProgramID, "texModulate")
            gph.uniformModelMtxLoc = glGetUniformLocation(gph.shaderProgramID, "model")
            glGetIntegerv(GL_MAX_VERTEX_ATTRIBS, addr gph.maxVertexAttributes)
            glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
            glEnable(GL_BLEND)
            gph.initialized = true
        else:
            echo "Shader sources not found (gph init)"
    else:
        echo "Load GL failed (gph init)"
    
proc new_graphics*(color:Rgba):Graphics =
    result = Graphics()
    #result.buffSet = @[]
    result.clearColor = color

proc init*(viewp:Viewport, gph:Graphics, width, height:GLuint, x:GLint = 0, y:GLint = 0) =
    glViewport(x, y, width, height)
    viewp.uniformProjMtx = orthoProjection(0.0, GLfloat(width), GLfloat(height), 0.0, - 1.0, 1.0)
    viewp.uniformProjectionLoc = glGetUniformLocation(gph.shaderProgramID, "projection")
    viewp.uniformViewLoc = glGetUniformLocation(gph.shaderProgramID, "view") 

proc shape_to_vertices*(buff:Buffer, shp:Shape, mat:Material) =
    case shp.kind
    of Rect:
        buff.vertices = @[
            GLfloat(0), 0, 0,
            mat.color.r, mat.color.g, mat.color.b, mat.color.a,
            shp.texCoords[0].x + mat.coordCorrection, shp.texCoords[0].y,
            shp.width, 0 , 0,
            mat.color.r, mat.color.g, mat.color.b, mat.color.a,
            shp.texCoords[1].x + mat.coordCorrection, shp.texCoords[1].y,
            shp.width, shp.height, 0,
            mat.color.r, mat.color.g, mat.color.b, mat.color.a,
            shp.texCoords[2].x + mat.coordCorrection, shp.texCoords[2].y,
            0, shp.height, 0,
            mat.color.r, mat.color.g, mat.color.b, mat.color.a,
            shp.texCoords[3].x + mat.coordCorrection, shp.texCoords[3].y
        ]
        buff.indices = @[GLuint(0), 1, 2, 2, 3, 0]

    of ConvexP:
        var targetIndice:GLuint = 1
        while targetIndice < GLuint(shp.vertices.high):
            buff.indices.add(0)
            buff.indices.add(targetIndice)
            targetIndice += 1
            buff.indices.add(targetIndice)

        if shp.vertexColor.len < shp.vertices.len:
            for i in shp.vertices:
                buff.vertices.add([float32(i.x), i.y, 0,
                                   mat.color.r, mat.color.g, mat.color.b, mat.color.a,
                                   0, 0])
            echo "vertexcolors skipped (shape_to_vertices)"
        else:
            for i in 0..shp.vertices.high:
                buff.vertices.add([float32(shp.vertices[i].x), shp.vertices[i].y, 0,
                                   shp.vertexColor[i].r, shp.vertexColor[i].g, 
                                   shp.vertexColor[i].b, shp.vertexColor[i].a, 0, 0])

    #echo "indices: ", buff.indices, " (shape_to_vertices)"
    #echo "vertices: ", buff.vertices, " (shape_to_vertices)"
    

proc add_vert*(gph:Graphics, buff:Buffer) =
    glGenVertexArrays(1, addr buff.VAO)
    glGenBuffers(1, addr buff.VBO)
    glGenBuffers(1, addr buff.EBO)
    glBindVertexArray(buff.VAO)
    
    glBindBuffer(GL_ARRAY_BUFFER, buff.VBO)
    glBufferData(GL_ARRAY_BUFFER, buff.vertices.len * sizeof(GLfloat), 
                 addr buff.vertices[0], GL_STATIC_DRAW)
    
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buff.EBO)
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, buff.indices.len * sizeof(GLuint), 
                 addr buff.indices[0], GL_STATIC_DRAW)
    # Position layout
    glVertexAttribPointer(0, POSITION_LENGTH, OGL_FLOAT, GL_FALSE, GLint(vertexLength * sizeof(GLfloat)),
                          nil)
    glEnableVertexAttribArray(0)
    # Color layout
    glVertexAttribPointer(1, COLOR_LENGTH, OGL_FLOAT, GL_FALSE, GLint(vertexLength * sizeof(GLfloat)), 
                          cast[pointer](colorDataOffset))
    glEnableVertexAttribArray(1)
    # Texcoord layout
    glVertexAttribPointer(2, TEX_COORD_LENGTH, OGL_FLOAT, GL_FALSE, GLint(vertexLength * sizeof(GLfloat)), 
                          cast[pointer](texcoordDataOffset))
    glEnableVertexAttribArray(2)
    # debind ?
    glBindBuffer(GL_ARRAY_BUFFER, 0)
    glBindVertexArray(0)
    buff.vertIdCount = GLint(buff.indices.len)
    #echo "vertexLength: ", vertexLength

proc add_texture*(buff:Buffer, tex:Image) =
    glGenTextures(1, addr buff.texture)
    glBindTexture(GL_TEXTURE_2D, buff.texture)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, GLint(tex.width), GLint(tex.height), 0, GLuint(GL_RGBA),
                 GL_UNSIGNED_BYTE, addr tex.data[0])
    glGenerateMipmap(GL_TEXTURE_2D)

#[
proc process*(gph:Graphics) =
    glClear(GL_COLOR_BUFFER_BIT)
    glUseProgram(gph.shaderProgramID)
    #glUniformMatrix4fv(gph.uniformProjectionLoc, 1, char(true), addr gph.uniformProjMtx[0])
    #glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
    for i in gph.viewportSet:
        glUniformMatrix4fv(i.uniformProjectionLoc, 1, GL_TRUE, addr i.uniformProjMtx[0])
        glUniformMatrix4fv(i.uniformViewLoc, 1, GL_FALSE, addr i.uniformViewMtx[0])
]#
proc clear*(gph:Graphics) =
    glClear(GL_COLOR_BUFFER_BIT)
    glUseProgram(gph.shaderProgramID)

proc update_view*(v:Viewport) =
    glUniformMatrix4fv(v.uniformProjectionLoc, 1, GL_TRUE, addr v.uniformProjMtx[0])
    glUniformMatrix4fv(v.uniformViewLoc, 1, GL_FALSE, addr v.uniformViewMtx[0])

proc draw*(gph:Graphics, buf:Buffer) =
    if buf.wireframe == true:
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE)

    else:
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL)

    #glUniform4f(gph.uniformColorLoc, i.uniformColor.r, i.uniformColor.g, i.uniformColor.b, i.uniformColor.a)
    glUniform1f(gph.uniformModulateLoc, buf.uniformModulation)
    #glUniform1f(gph.uniformTexModulateLoc, i.uniformTexModulation)
    glUniformMatrix4fv(gph.uniformModelMtxLoc, 1, GL_FALSE, addr buf.uniformModelMtx[0])
    glBindTexture(GL_TEXTURE_2D, buf.texture)
    glBindVertexArray(buf.VAO)
    glDrawElements(GL_TRIANGLES, buf.vertIdCount, GL_UNSIGNED_INT, nil)

proc remove*(buffer:Buffer) =
    glDeleteVertexArrays(1, addr buffer.VAO)
    glDeleteBuffers(1, addr buffer.VBO)
    glDeleteBuffers(1, addr buffer.EBO)
    #echo "A drawing is removed (graphics remove)"

#proc remove*(gph:Graphics, viewp:Viewport) =
#    gph.viewportSet.delete(gph.viewportSet.find(viewp))
    

