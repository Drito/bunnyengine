#e_objects.nim

import math, times

{.link: "c_libs.o".}

type
    GLfloat* = float32
    
    GLuint* = uint32
    
    GLint* = int32

    Vec2i* = object
        x:int
        y:int
    
    Vec2f* = object
        x*:float
        y*:float

    Vec3f* = object
        x, y, z:float
        
    Mat4x4* = array[16, GLfloat]

    Rgba* = object
        r*, g*, b*, a*:float
    
    ShapeEnum* = enum
        Rect,
        ConvexP

    Shape* = object
        texCoords*:seq[Vec2f]
        case kind*:ShapeEnum
        of Rect:
            width*:float
            height*:float
        of ConvexP:
            vertices*:seq[Vec2f]
            vertexColor*:seq[Rgba]

    Image* = ref object
        file*:string
        width*, height*:GLint
        channelCount*:GLint
        data*:seq[char]

    Material* = ref object
        color*:Rgba
        tex*:Image
        vertColModulation*:float
        texModulation*:float
        coordCorrection*:float
        wireframe*:bool
    
    Timer* = ref object
        initialTime:float
        #elapsed:int
        
let 
    defaultRectTexcoords:seq[Vec2f] = @[Vec2f(x:0.0,y:0.0),
                                        Vec2f(x:1.0,y:0.0),
                                        Vec2f(x:1.0,y:1.0),
                                        Vec2f(x:0.0,y:1.0)]

var  identityMat4x4*:Mat4x4 = [GLfloat(1), 0, 0, 0, 
                                       0, 1, 0, 0, 
                                       0, 0, 1, 0, 
                                       0, 0, 0, 1]

proc to_radians*(degr:float):float =
    return PI * degr / 180

proc vec2f*(xArg:float, yArg:float):Vec2f =
    result = Vec2f(x: xArg, y: yArg)

proc vec2f*(mat:Mat4x4):Vec2f =
    result = Vec2f(x: mat[12], y: mat[13])

proc `+`*(a:Vec2f, b:Vec2f):Vec2f =
    result.x = a.x + b.x
    result.y = a.y + b.y

proc `-`*(a:Vec2f, b:Vec2f):Vec2f =
    result.x = a.x - b.x
    result.y = a.y - b.y

proc `*`*(a:Vec2f, b:float):Vec2f =
    result.x = a.x * b
    result.y = a.y * b

proc norm*(v:Vec2f):Vec2f =
    var m = sqrt(v.x*v.x + v.y*v.y)
    if m != 0:
        result.x = v.x / m
        result.y = v.y / m
    else:
        # avoid division with 0
        result.x = 0.0
        result.y = 0.0

proc distance*(a:Vec2f, b:Vec2f):float =
    var 
        sideA = a.x - b.x
        sideB = a.y - b.y
    return sqrt(sideA * sideA) + (sideB * sideB)

proc rgb*(r,g,b:GLfloat):Rgba =
    result.r = r
    result.g = g
    result.b = b
    result.a = 1.0
    
proc rgba*(r,g,b,a:GLfloat):Rgba =
    result.r = r
    result.g = g
    result.b = b
    result.a = a    
    
proc convexP*(vert:seq[Vec2f], vertColors:seq[Rgba]= @[]):Shape =
    result = Shape(kind:ConvexP, vertices:vert)
    result.vertexColor = vertColors

proc rect*(w:float, h:float, tc:seq[Vec2f] = @[Vec2f(x:0.0,y:0.0),
                                               Vec2f(x:1.0,y:0.0),
                                               Vec2f(x:1.0,y:1.0),
                                               Vec2f(x:0.0,y:1.0)]):Shape =
    return Shape(kind:Rect, width:w, height:h, texCoords:tc)
#[
proc rect*(im:Image, tc:seq[Vec2f] = @[Vec2f(x:0.0,y:0.0),
                                       Vec2f(x:1.0,y:0.0),
                                       Vec2f(x:1.0,y:1.0),
                                       Vec2f(x:0.0,y:1.0)]):Shape =
    return Shape(kind: Rect, width: im.width, height: im.height, texCoords:tc)
]#
    
proc orthoProjection*(left, right, bottom, top, far, near:float):Mat4x4 =
    result = identityMat4x4
    result[0] = GLfloat(2.0 / (right - left))
    result[5] = GLfloat(2.0 / (top - bottom))
    result[3] = GLfloat(- ((right + left) / (right - left)))
    result[7] = GLfloat(- ((top + bottom) / (top - bottom)))
    result[10] = GLfloat(-2 / (far - near))
    result[11] = GLfloat(-((far + near)/(far - near)))
    
    # Version with transposition in column-major order.
    #result[0] = OGLfloat(2.0 / (right - left))
    #result[5] = OGLfloat(2.0 / (top - bottom))
    #result[12] = OGLfloat(- ((right + left) / (right - left)))
    #result[13] = OGLfloat(- ((top + bottom) / (top - bottom)))
    #result[10] = OGLfloat(-2 / (far - near))
    #result[14] = OGLfloat(-((far + near)/(far - near)))
    
proc `*`*(a, b:Mat4x4):Mat4x4 =
    result[0] = a[0] * b[0] + a[4] * b[1] + a[8] * b[2] + a[12] * b[3]
    result[1] = a[1] * b[0] + a[5] * b[1] + a[9] * b[2] + a[13] * b[3]   
    result[2] = a[2] * b[0] + a[6] * b[1] + a[10] * b[2] + a[14] * b[3]
    result[3] = a[3] * b[0] + a[7] * b[1] + a[11] * b[2] + a[15] * b[3]
    
    result[4] = a[0] * b[4] + a[4] * b[5] + a[8] * b[6] + a[12] * b[7]
    result[5] = a[1] * b[4] + a[5] * b[5] + a[9] * b[6] + a[13] * b[7]
    result[6] = a[2] * b[4] + a[6] * b[5] + a[10] * b[6] + a[14] * b[7]
    result[7] = a[3] * b[4] + a[7] * b[5] + a[11] * b[6] + a[15] * b[7] 
    
    result[8] = a[0] * b[8] + a[4] * b[9] + a[8] * b[10] + a[12] * b[11]
    result[9] = a[1] * b[8] + a[5] * b[9] + a[9] * b[10] + a[13] * b[11]
    result[10] = a[2] * b[8] + a[6] * b[9] + a[10] * b[10] + a[14] * b[11]
    result[11] = a[3] * b[8] + a[7] * b[9] + a[11] * b[10] + a[15] * b[11]
    
    result[12] = a[0] * b[12] + a[4] * b[13] + a[8] * b[14] + a[12] * b[15]
    result[13] = a[1] * b[12] + a[5] * b[13] + a[9] * b[14] + a[13] * b[15]
    result[14] = a[2] * b[12] + a[6] * b[13] + a[10] * b[14] + a[14] * b[15]
    result[15] = a[3] * b[12] + a[7] * b[13] + a[11] * b[14] + a[15] * b[15]
    
proc rotationZ*(rad:float):Mat4x4 = #{.noInit.} =
    let
        s = GLfloat(sin(rad))
        c = GLfloat(cos(rad))
    result = [c, s, 0, 0,
            - s, c, 0, 0,
              0, 0, 1, 0,
              0, 0, 0, 1]

#[
c -s  0  0
s  c  0  0
0  0  1  0
0  0  0  1

]#


proc translation*(dir:Vec2f):Mat4x4 = #{.noInit.} =
    ## Returns a new translation matrix from a vector.
    result = [GLfloat(1), 0, 0, 0,
                      0, 1, 0, 0,
                      0, 0, 1, 0,
                      dir.x, dir.y, 0, 1]

#[

1  0  0  dir.x
0  1  0  dir.y
0  0  1  0
0  0  0  1

]#

proc translate*(mat:var Mat4x4, vec:Vec2f) =
    mat[12] = vec.x
    mat[13] = vec.y

proc scaling*(s:Vec2f):Mat4x4 =
    ## Returns a new scaling matrix from x and y scale.
    result = [GLfloat(s.x), 0, 0, 0, 
                      0, s.y, 0, 0, 
                      0, 0, 1, 0, 
                      0, 0, 0, 1]

#[
s.x 0   0  0
0   s.y 0  0
0   0   1  0
0   0   0  1
]#

proc material*(col:Rgba = rgb(1.0, 1.0, 1.0)):Material =
    result = Material(tex: Image(width: 0, height: 0, channelCount: 0, data: @[]))
    result.color = col

proc start*(timer:Timer) =
    timer.initialTime = epochTime()

proc elapsed_sec*(timer:Timer):float =
    ## return elapsed time in seconds
    return epochTime() - timer.initialTime

proc elapsed_ms*(timer:Timer):int =
    ## return elapsed time in milliseconds
    return int((epochTime() * 1000) - timer.initialTime * 1000)

