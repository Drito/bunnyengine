GLFW = --passL:'`pkg-config glfw3 --static --cflags --libs`'

GLEW = --passL:'`pkg-config glew --static --cflags --libs`'

PATHS = --passC: '-I Libs/Lodepng/' \
	   --path: Src/System/GLFW/\
	   --path: Src/GlLoader/Glew/ \
	   --path: Src \
	   --path: Src/Graphics/Opengl3.3/  \
	   --path: Src/ImageLoader/Lodepng/ \
	   --path: Libs/Lodepng/ 

all: c_lib
	nim c --nilseqs:on --out:Out/bunnymark $(GLFW) $(GLEW) $(PATHS) Src/bunnymark.nim

c_lib: ; gcc -Wall -c Src/c_libs.c

run:
	Out/bunnymark

clean:
	rm c_libs.o

test: c_lib
	nim c -r $(GLEW) $(GLFW) $(PATHS) Test/bunnymark.nim
